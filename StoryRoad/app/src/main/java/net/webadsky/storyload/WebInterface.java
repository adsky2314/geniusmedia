package net.webadsky.storyload;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.webkit.JavascriptInterface;

public class WebInterface {
    private Context mContext;
    private MainActivity mMainActivity;
    final private Handler handler = new Handler();
    public WebInterface(MainActivity mainActivity, Context context) {
        this.mMainActivity = mainActivity;
        this.mContext = context;
    }

    @JavascriptInterface
    public String getToken() {
        String token = "";
        SharedPreferences spf = mContext.getSharedPreferences(mContext.getPackageName(), Context.MODE_PRIVATE);
        token = spf.getString(PrefsConstants.PROPERTY_FCM_TOKEN, "");

        return token;
    }

    @JavascriptInterface
    public void openYoutube(final String id) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                mMainActivity.openYouTube(id);
            }
        });
    }
}
