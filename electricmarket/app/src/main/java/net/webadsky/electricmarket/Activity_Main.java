package net.webadsky.electricmarket;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.DownloadListener;
import android.webkit.GeolocationPermissions;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Stack;

import static java.lang.System.exit;

public class Activity_Main extends AppCompatActivity {


    private final String TAG = "Activity_Main";
    public Activity activity;

    private WebView mWebView;

    private ValueCallback<Uri> filePathCallbackNormal;
    private ValueCallback<Uri[]> filePathCallbackLollipop;
    private final static int FILECHOOSER_NORMAL_REQ_CODE = 1;
    private final static int FILECHOOSER_LOLLIPOP_REQ_CODE = 2;

    private final static String BASE_URL = "http://electricmarket.webadsky.net/gn";

    private Uri cameraImageUri = null;

    final private CookieManager cookieManager = CookieManager.getInstance();

    private Stack<WebView> childView;
    private WebView CurrentWebView;
    private RelativeLayout webRoot;

    //로그인 정보
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    private static final int MY_PERMISSION_REQUEST_LOCATION = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        setUI();
        setInit();
        setEvent();
        setWebView();

    }


    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.getInstance().startSync();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.getInstance().stopSync();
        }
    }

    @Override
    public void onBackPressed() {

        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            backPressCloseHandler.onBackPressed();
        }

    }

    private void setUI() {

        mWebView = findViewById(R.id.webView);
        webRoot = findViewById(R.id.webRoot);

    }

    private void setInit() {

        activity = Activity_Main.this;

        childView = new Stack<>();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.createInstance(this);
        }
        cookieManager.setAcceptCookie(true);

        backPressCloseHandler = new BackPressCloseHandler(this);

    }

    private void setEvent() {

    }

    private void setWebView() {

        mWebView.setWebViewClient(new MyWebViewClient());
        mWebView.setWebChromeClient(new MyChromeClient());

        final WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setUseWideViewPort(true);
        settings.setJavaScriptEnabled(true);

        if (Build.VERSION.SDK_INT >= 21) {
            mWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            mWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        settings.setGeolocationEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setSupportMultipleWindows(true);
        settings.setGeolocationEnabled(true);

        settings.setAppCacheEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setDomStorageEnabled(true);



        mWebView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(final String url, final String userAgent, final String contentDisposition, final String mimeType, long contentLength) {
                PermissionListener permissionListener = new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {

                        if (Build.VERSION.SDK_INT >= 23 &&
                                ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    0);
                        } else {

                            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));


                            request.setMimeType(mimeType);
                            //------------------------COOKIE!!------------------------
                            String cookies = CookieManager.getInstance().getCookie(url);
                            request.addRequestHeader("cookie", cookies);
                            //------------------------COOKIE!!------------------------
                            request.addRequestHeader("User-Agent", userAgent);
                            request.setDescription("Downloading file...");
                            request.setTitle(URLUtil.guessFileName(url, contentDisposition, mimeType));

                            request.allowScanningByMediaScanner();
                            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(url, contentDisposition, mimeType));
                            DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                            dm.enqueue(request);
                            Toast.makeText(getApplicationContext(), "Downloading File", Toast.LENGTH_LONG).show();

                        }
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                    }
                };

                TedPermission.with(activity)
                        .setPermissionListener(permissionListener)
                        .setDeniedMessage("[설정] > [권한] 에서 권한을 허용할 수 있습니다.")
                        .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .check();
            }
        });


        childView.push(mWebView);
        CurrentWebView = mWebView;
        CurrentWebView.loadUrl(BASE_URL);

        registerForContextMenu(mWebView);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILECHOOSER_NORMAL_REQ_CODE) {
            if (filePathCallbackNormal == null) return;
            Uri result = (data == null || resultCode != RESULT_OK) ? null : data.getData();
            filePathCallbackNormal.onReceiveValue(result);
            filePathCallbackNormal = null;
        } else if (requestCode == FILECHOOSER_LOLLIPOP_REQ_CODE) {
            if (filePathCallbackLollipop == null) return;

            if (data == null)
                data = new Intent();
            if (data.getData() == null)
                data.setData(cameraImageUri);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                filePathCallbackLollipop.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, data));
            }
            filePathCallbackLollipop = null;
        }
    }

    private void runCamera(boolean _isCapture) {
        if (!_isCapture) {
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("*/*");
            startActivityForResult(Intent.createChooser(i, "File Chooser"), FILECHOOSER_LOLLIPOP_REQ_CODE);
            return;
        }

        Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        File path = getFilesDir();
        File file = new File(path, "capture.png");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            String strpa = getApplicationContext().getPackageName();
            cameraImageUri = FileProvider.getUriForFile(this, strpa + ".fileprovider", file);
        } else {
            cameraImageUri = Uri.fromFile(file);
        }
        intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, cameraImageUri);

        if (!_isCapture) {
            Intent pickIntent = new Intent(Intent.ACTION_PICK);
            pickIntent.setType(MediaStore.Images.Media.CONTENT_TYPE);
            pickIntent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            String pickTitle = "사진 가져올 방법을 선택하세요.";
            Intent chooserIntent = Intent.createChooser(pickIntent, pickTitle);

            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Parcelable[]{intentCamera});
            startActivityForResult(chooserIntent, FILECHOOSER_LOLLIPOP_REQ_CODE);
        } else {
            startActivityForResult(intentCamera, FILECHOOSER_LOLLIPOP_REQ_CODE);
        }
    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError er) {
            final SslErrorHandler sslErrorHandler = handler;
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setMessage("인증서 오류가 발생하였습니다.\n서버설정이 잘못되었거나, 불법사용자가 연결을 가로채고 있을 수 있습니다.\n연결하시겠습니까?");
            builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    sslErrorHandler.proceed();
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton("취소", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    sslErrorHandler.cancel();
                    dialog.dismiss();
                    exit(0);
                }
            });
            builder.show();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            Log.d("url", String.valueOf(url));

            if (url.startsWith("http:") || url.startsWith("https:")) {
                if (url.endsWith(".pdf") || url.startsWith("https://m.youtube.com")) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                    return true;
                }
                return false;
            } else if (url.startsWith("kakaotalk:")) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
                return true;
            } else if (url.startsWith("tel:")) {
                Intent tel = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                startActivity(tel);
                return true;
            } else if (url.startsWith("sms:")) {
                Intent sms = new Intent(Intent.ACTION_SENDTO, Uri.parse(url));
                try {
                    sms.putExtra("sms_body", URLDecoder.decode(url.substring(url.indexOf("?body=") + 6), "utf-8"));
                    startActivity(sms);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return true;
            } else if (url.startsWith("intent:")) {
                try {
                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                    Intent existPackage = getPackageManager().getLaunchIntentForPackage(intent.getPackage());
                    if (existPackage != null) {
                        intent.setAction(Intent.ACTION_VIEW);
                        // forbid launching activities without BROWSABLE category
                        intent.addCategory("android.intent.category.BROWSABLE");
                        // forbid explicit call
                        intent.setComponent(null);
                        // forbid Intent with selector Intent
                        intent.setSelector(null);
                        startActivity(intent);
                    } else {
                        Intent marketIntent = new Intent(Intent.ACTION_VIEW);
                        marketIntent.setData(Uri.parse("market://details?id=" + intent.getPackage()));
                        startActivity(marketIntent);
                    }
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (url.startsWith("market:")) {
                try {
                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                    if (intent != null) {
                        // forbid launching activities without BROWSABLE category
                        intent.addCategory("android.intent.category.BROWSABLE");
                        // forbid explicit call
                        intent.setComponent(null);
                        // forbid Intent with selector Intent
                        intent.setSelector(null);
                        startActivity(intent);
                    }
                    return true;
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            } else if (url.startsWith("camera")) {
                try {
                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                    if (intent != null) {
                        // forbid launching activities without BROWSABLE category
                        intent.addCategory("android.intent.category.BROWSABLE");
                        // forbid explicit call
                        intent.setComponent(null);
                        // forbid Intent with selector Intent
                        intent.setSelector(null);
                        startActivity(intent);
                    }
                    return true;
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                        }
                    });
                }
            }, 70);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                cookieManager.setAcceptThirdPartyCookies(view, true);
        }

    }

    private class MyChromeClient extends WebChromeClient {

        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            AlertDialog.Builder alertB = new AlertDialog.Builder(activity);
            alertB.setMessage(message);
            alertB.setTitle("");
            alertB.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertB.show();
            result.confirm();
            return true;
        }

        @Override
        public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
            AlertDialog.Builder alertB = new AlertDialog.Builder(activity);
            alertB.setMessage(message);
            alertB.setTitle("");

            alertB.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    result.confirm();
                }
            });
            alertB.setNegativeButton("취소", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    result.cancel();
                }
            });
            alertB.show();
            return true;
        }

        @Override
        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            return super.onConsoleMessage(consoleMessage);
        }

        public void onGeolocationPermissionsShowPrompt(final String origin,final GeolocationPermissions.Callback callback) {
            final String myOrigin = origin;
            final GeolocationPermissions.Callback myCallback = callback;

            myCallback.invoke(myOrigin, true, true);


//            Log.i(TAG, "onGeolocationPermissionsShowPrompt()");
//
//            final boolean remember = false;
//            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//            builder.setTitle("위치");
//            builder.setMessage("기기의 위치에 액세스 하려고 합니다.")
//                    .setCancelable(true).setPositiveButton("허용", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    // origin, allow, remember
//                    callback.invoke(origin, true, remember);
//                }
//            }).setNegativeButton("차단", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    // origin, allow, remember
//                    callback.invoke(origin, false, remember);
//                }
//            });
//            AlertDialog alert = builder.create();
//            alert.show();

        }

        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
            filePathCallbackNormal = uploadMsg;
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("*/*");

            startActivityForResult(Intent.createChooser(i, "File Chooser"), FILECHOOSER_NORMAL_REQ_CODE);
        }

        // For Android 5.0+
        public boolean onShowFileChooser(
                WebView webView, ValueCallback<Uri[]> filePathCallback,
                FileChooserParams fileChooserParams) {
            if (filePathCallbackLollipop != null) {
                filePathCallbackLollipop.onReceiveValue(null);
                filePathCallbackLollipop = null;
            }
            filePathCallbackLollipop = filePathCallback;

            boolean isCapture = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                isCapture = fileChooserParams.isCaptureEnabled();
            }
            runCamera(isCapture);

            return true;
        }

        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {


            WebView nWebView = new WebView(view.getContext());
            final WebSettings settings = nWebView.getSettings();

            settings.setJavaScriptEnabled(true);
            settings.setUseWideViewPort(true);
            settings.setSupportMultipleWindows(true);
            settings.setJavaScriptCanOpenWindowsAutomatically(true);
            settings.setCacheMode(WebSettings.LOAD_DEFAULT);
            nWebView.setWebViewClient(new MyWebViewClient());
            nWebView.setWebChromeClient(new MyChromeClient());
            if (Build.VERSION.SDK_INT >= 21) {
                nWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            } else {
                nWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            }
            settings.setGeolocationEnabled(true);
            settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
            settings.setSupportMultipleWindows(true);


            webRoot.addView(nWebView);
            childView.push(CurrentWebView);
            CurrentWebView = nWebView;

            nWebView.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
            Animation animation = AnimationUtils.loadAnimation(getBaseContext(), R.anim.right_in);
            animation.setStartOffset(0);
            nWebView.startAnimation(animation);
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(nWebView);
            resultMsg.sendToTarget();
            return true;
        }

        @Override
        public void onCloseWindow(WebView window) {

            if (CurrentWebView != mWebView) {
                Animation animation = AnimationUtils.loadAnimation(getBaseContext(), R.anim.right_out);
                animation.setStartOffset(0);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (CurrentWebView != mWebView) {
                            webRoot.removeView(CurrentWebView);
                            CurrentWebView.destroy();
                            CurrentWebView = childView.pop();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                CurrentWebView.startAnimation(animation);
            }

        }

    }

    private boolean doubleBackToExitPressedOnce;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (doubleBackToExitPressedOnce) {
                backPressCloseHandler.onBackPressed();
                return false;
            }
            this.doubleBackToExitPressedOnce = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 800);
            if (CurrentWebView.canGoBack()) {
                CurrentWebView.goBack();
                return true;
            } else if (CurrentWebView != mWebView) {
                Animation animation = AnimationUtils.loadAnimation(getBaseContext(), R.anim.right_out);
                animation.setStartOffset(0);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (CurrentWebView != mWebView) {
                            webRoot.removeView(CurrentWebView);
                            CurrentWebView.destroy();
                            CurrentWebView = childView.pop();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                CurrentWebView.startAnimation(animation);
                return true;
            } else {
                backPressCloseHandler.onBackPressed();
                return false;
            }
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }


    protected BackPressCloseHandler backPressCloseHandler;

    private class BackPressCloseHandler {

        private long backKeyPressedTime = 0;
        private Toast toast;

        private Activity activity;

        public BackPressCloseHandler(Activity context) {
            this.activity = context;
        }

        public void onBackPressed() {
            if (System.currentTimeMillis() > backKeyPressedTime + 2000) {
                backKeyPressedTime = System.currentTimeMillis();
                toast = Toast.makeText(activity, "종료하시려면 뒤로가기 버튼을 한번 더 눌러주세요.",
                        Toast.LENGTH_SHORT);
                toast.show();
                return;
            }
            if (System.currentTimeMillis() <= backKeyPressedTime + 2000) {
                activity.finish();
                toast.cancel();
            }
        }

    }

}
