package net.webadsky.electricmarket;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;


public class Activity_Intro extends AppCompatActivity {

    private final String TAG = "Activity_Intro";
    public Activity activity;

    //로그인 정보
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    private Handler hIntro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        setUI();
        setInit();
        setEvent();

    }



    private void setUI() {

    }

    private void setInit() {

        activity = Activity_Intro.this;

        hIntro = new Handler();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId  = "default_notification_channel_id";
            String channelName = "default_push";
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        }


        hIntro.postDelayed(rIntro, 1500);

    }

    private void setEvent() {

    }



    private void checkFcmId() {

        moveToLogin();


    }


    private Runnable rIntro = new Runnable() {
        @Override
        public void run() {

            checkFcmId();

        }
    };


    private void moveToLogin() {

        Intent intent = new Intent(activity, Activity_Main.class);
        startActivity(intent);
        finish();
    }


}
